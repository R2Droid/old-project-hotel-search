package com.radek.searchhotel.testData;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;

import com.radek.searchhotel.utils.TestUtils;
import com.radek.searchhotel.model.DbContract.*;
import com.radek.searchhotel.model.DbHelper;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by Radek on 2015-12-30.
 */
public class TestDatabase extends AndroidTestCase {
    DbHelper testDbHelper;
    RenamingDelegatingContext testContext;
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        testContext = new RenamingDelegatingContext(getContext(), "SQLiteTest");
        testDbHelper = new DbHelper(testContext);
        SQLiteDatabase sqLiteDatabase = testDbHelper.getWritableDatabase();
        sqLiteDatabase.delete(HotelsEntry.TABLE_NAME, null, null);
        sqLiteDatabase.delete(RoomsEntry.TABLE_NAME, null, null);
    }

    @Override
    protected void tearDown() throws Exception {
        testDbHelper.close();
        SQLiteDatabase sqLiteDatabase = testDbHelper.getWritableDatabase();
        sqLiteDatabase.delete(HotelsEntry.TABLE_NAME, null, null);
        sqLiteDatabase.delete(RoomsEntry.TABLE_NAME, null, null);
        super.tearDown();
    }

    public void testCreateDatabase() {
        SQLiteDatabase testDb = testDbHelper.getWritableDatabase();

        // Table names HashSet
        final HashSet<String> tableNameSet = new HashSet<>();
        tableNameSet.add(RoomsEntry.TABLE_NAME);
        tableNameSet.add(HotelsEntry.TABLE_NAME);

        Cursor tableNames = testDb.rawQuery("SELECT name FROM sqlite_master WHERE type = 'table'", null);
        assertTrue("Database table names cannot be retrieved", tableNames.moveToFirst());


        // Cursor 'tableNames' should contain names of tables and match HashSet elements leaving it empty
        do {
            tableNameSet.remove(tableNames.getString(0));
        } while (tableNames.moveToNext());

        assertTrue("HashSet should be empty", tableNameSet.isEmpty());
        tableNames.close();

    }

    public void testHotelColumns() {
        SQLiteDatabase testDb = testDbHelper.getWritableDatabase();

        // 'hotel' table columns HashSet
        final HashSet<String> hotelColumnNamesSet = new HashSet<>();
        hotelColumnNamesSet.add(HotelsEntry._ID);
        hotelColumnNamesSet.add(HotelsEntry.TITLE_COLUMN);
        hotelColumnNamesSet.add(HotelsEntry.COUNTRY_COLUMN);
        hotelColumnNamesSet.add(HotelsEntry.CITY_COLUMN);
        hotelColumnNamesSet.add(HotelsEntry.STREET_COLUMN);
        hotelColumnNamesSet.add(HotelsEntry.LATITUDE_COORD_COLUMN);
        hotelColumnNamesSet.add(HotelsEntry.LONGITUDE_COORD_COLUMN);
        hotelColumnNamesSet.add(HotelsEntry.RATING_COLUMN);
        hotelColumnNamesSet.add(HotelsEntry.SHORT_DESCRIPTION_COLUMN);
        hotelColumnNamesSet.add(HotelsEntry.STARS_COLUMN);
        hotelColumnNamesSet.add(HotelsEntry.PICTURES_COLUMN);
        hotelColumnNamesSet.add(HotelsEntry.MODIFY_DATE);

        Cursor hotelColumnNames = testDb.rawQuery("PRAGMA table_info (" + HotelsEntry.TABLE_NAME + ")", null);
        assertTrue("'hotel' table column names cannot be retrieved", hotelColumnNames.moveToFirst());
        // Cursor 'hotelColumnNames' should contain names of all columns and match HashSet elements leaving HashSet empty
        String cursorContents = DatabaseUtils.dumpCursorToString(hotelColumnNames);

        int hotelNameColumnIndex = hotelColumnNames.getColumnIndex("name");

        do {
            String hotelColumnName = hotelColumnNames.getString(hotelNameColumnIndex);
            hotelColumnNamesSet.remove(hotelColumnName);
        } while (hotelColumnNames.moveToNext());

        assertTrue(
                "Error: 'hotel' table has incorrect number of columns or the names are invalid",
                hotelColumnNamesSet.isEmpty()
        );
        hotelColumnNames.close();
        testDb.close();

    }

    public void testRoomColumns() {
        SQLiteDatabase testDb = testDbHelper.getWritableDatabase();
        // 'room' table columns HashSet
        final HashSet<String> roomColumnNamesSet = new HashSet<>();
        roomColumnNamesSet.add(RoomsEntry._ID);
        roomColumnNamesSet.add(RoomsEntry.HOTEL_ID);
        roomColumnNamesSet.add(RoomsEntry.TITLE_COLUMN);
        roomColumnNamesSet.add(RoomsEntry.CAPACITY_COLUMN);
        roomColumnNamesSet.add(RoomsEntry.PRICE_COLUMN);
        roomColumnNamesSet.add(RoomsEntry.DESCRIPTION_COLUMN);
        roomColumnNamesSet.add(RoomsEntry.PICTURES_COLUMN);
        roomColumnNamesSet.add(RoomsEntry.MODIFY_DATE);

        Cursor roomColumnNames = testDb.rawQuery("PRAGMA table_info (" + RoomsEntry.TABLE_NAME + ")", null);
        assertTrue("'room' table column names cannot be retrieved", roomColumnNames.moveToFirst());

        // Cursor 'roomColumnNames' should contain names of all columns and match HashSet elements leaving HashSet empty
        int roomNameColumnIndex = roomColumnNames.getColumnIndex("name");

        do {
            String roomColumnName = roomColumnNames.getString(roomNameColumnIndex);
            roomColumnNamesSet.remove(roomColumnName);
        } while (roomColumnNames.moveToNext());

        assertTrue(
                "Error: 'room' table has incorrect number of columns or the names are invalid",
                roomColumnNamesSet.isEmpty()
        );
        roomColumnNames.close();
        testDb.close();
    }


    public void testHotelSingleInsert() {
        SQLiteDatabase testDb = testDbHelper.getWritableDatabase();
        long testRowId;

        ContentValues hotelTestContentValues = TestUtils.getSingleHotelContentValues(testContext);
        testRowId = testDb.insert(HotelsEntry.TABLE_NAME, null, hotelTestContentValues);

        assertTrue("Error: single insert to hotel table failed", testRowId != -1L);

        Set<Map.Entry<String, Object>> testSet = hotelTestContentValues.valueSet();
        Cursor cursor = testDb.query(HotelsEntry.TABLE_NAME, null, null, null, null, null, null);
        assertTrue("Error: hotel database query failed", cursor.moveToFirst());

        TestUtils.validateRecord(cursor, hotelTestContentValues);

        cursor.close();
        testDb.close();
    }

    public void testRoomSingleInsert() {
//      ContentValues toCompareContentValues = new ContentValues();
        long testRowId;
        Cursor cursor;

        SQLiteDatabase testDb = testDbHelper.getWritableDatabase();
        ContentValues roomTestContentValues = TestUtils.getSingleRoomContentValues(testContext);

        testRowId = testDb.insert(RoomsEntry.TABLE_NAME, null, roomTestContentValues);
        assertTrue("Error: single insert to room table failed", testRowId != -1L);

        cursor = testDb.query(RoomsEntry.TABLE_NAME, null, null, null, null, null, null);
        assertTrue("Error: room database query failed", cursor.moveToFirst());

        TestUtils.validateRecord(cursor, roomTestContentValues);

        cursor.close();
        testDb.close();
    }

}






















