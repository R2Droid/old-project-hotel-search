package com.radek.searchhotel.testData;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.test.AndroidTestCase;

import com.radek.searchhotel.model.DbContract;
import com.radek.searchhotel.utils.TestUtils;
import com.radek.searchhotel.model.DbHelper;
import com.radek.searchhotel.model.DbContract.HotelsEntry;
import com.radek.searchhotel.model.DbContract.RoomsEntry;

/**
 * Created by Radek on 2016-01-14.
 */
public class TestProvider extends AndroidTestCase {
    final String TAG = TestProvider.class.getSimpleName();
    Context testContext;
    SQLiteOpenHelper sqLiteOpenHelper;


    @Override
    protected void setUp() throws Exception {
        super.setUp();
        testContext = getContext();
        sqLiteOpenHelper = new DbHelper(getContext());
        SQLiteDatabase sqLiteDatabase = sqLiteOpenHelper.getWritableDatabase();
        sqLiteDatabase.delete(HotelsEntry.TABLE_NAME, null, null);
        sqLiteDatabase.delete(RoomsEntry.TABLE_NAME, null, null);
    }

    @Override
    protected void tearDown() throws Exception {
        SQLiteDatabase sqLiteDatabase = sqLiteOpenHelper.getWritableDatabase();
        sqLiteDatabase.delete(HotelsEntry.TABLE_NAME, null, null);
        sqLiteDatabase.delete(RoomsEntry.TABLE_NAME, null, null);
        super.tearDown();
    }


    public void testHotelProviderQuery() {
        long insertId;
        Cursor cursor;
        SQLiteDatabase sqLiteDatabase = sqLiteOpenHelper.getWritableDatabase();

        ContentValues contentValues = TestUtils.getSingleHotelContentValues(getContext());
        insertId = sqLiteDatabase.insert(HotelsEntry.TABLE_NAME, null, contentValues);

        assertTrue(TAG + ": single insert to hotel table failed", insertId != -1L);

        cursor = getContext().getContentResolver().query(HotelsEntry.CONTENT_URI, null, null, null, null);

        assertTrue(TAG + ", QueryTest: hotel cursor is empty", cursor.moveToFirst());
        TestUtils.validateRecord(cursor, contentValues);
        sqLiteDatabase.delete(HotelsEntry.TABLE_NAME, null, null);

        cursor.close();
    }

    public void testRoomProviderQuery() {
        long insertId;
        Cursor cursor;
        SQLiteDatabase sqLiteDatabase = sqLiteOpenHelper.getWritableDatabase();

        ContentValues contentValues = TestUtils.getSingleRoomContentValues(getContext());
        insertId = sqLiteDatabase.insert(RoomsEntry.TABLE_NAME, null, contentValues);

        assertTrue(TAG + ", QueryTest: room single insert to room table failed", insertId != -1L);

        cursor = getContext().getContentResolver().query(RoomsEntry.CONTENT_URI, null, null, null, null);

        assertTrue(TAG + ": cursor is empty", cursor.moveToFirst());
        TestUtils.validateRecord(cursor, contentValues);
        sqLiteDatabase.delete(RoomsEntry.TABLE_NAME, null, null);

        cursor.close();
    }

    public void testHotelProviderInsert() {
        Uri retUri;
        Cursor cursor;
        ContentValues contentValues = TestUtils.getSingleHotelContentValues(getContext());

        retUri = getContext().getContentResolver().insert(HotelsEntry.CONTENT_URI, contentValues);
        getContext().getContentResolver().notifyChange(retUri, null); // code insert in provider

        cursor = getContext().getContentResolver().query(HotelsEntry.CONTENT_URI, null, null, null, null);

        assertTrue(TAG + ", InsertTest: hotel cursor is empty", cursor.moveToFirst());
        TestUtils.validateRecord(cursor, contentValues);

        SQLiteDatabase sqLiteDatabase = sqLiteOpenHelper.getWritableDatabase();
        sqLiteDatabase.delete(HotelsEntry.TABLE_NAME, null, null);

        cursor.close();
    }

    public void testRoomProviderInsert() {
        Uri retUri;
        Cursor cursor;
        ContentValues contentValues = TestUtils.getSingleRoomContentValues(getContext());

        retUri = getContext().getContentResolver().insert(RoomsEntry.CONTENT_URI, contentValues);
        getContext().getContentResolver().notifyChange(retUri, null);

        cursor = getContext().getContentResolver().query(RoomsEntry.CONTENT_URI, null, null, null, null);

        assertTrue(TAG + ", InsertTest: room cursor is empty", cursor.moveToFirst());
        TestUtils.validateRecord(cursor, contentValues);

        SQLiteDatabase sqLiteDatabase = sqLiteOpenHelper.getWritableDatabase();
        sqLiteDatabase.delete(RoomsEntry.TABLE_NAME, null, null);

        cursor.close();
    }


    public void testHotelItemProviderDelete() {
        int testId;
        int retDelete;
        Cursor insertCheckCursor;
        Cursor deleteCheckCursor;
        ContentValues contentValues = TestUtils.getSingleHotelContentValues(getContext());


        getContext().getContentResolver().insert(HotelsEntry.CONTENT_URI, contentValues);
        insertCheckCursor = getContext().getContentResolver().query(HotelsEntry.CONTENT_URI, null, null, null, null);

        assertTrue(TAG + ", InsertTest: hotel cursor is empty", insertCheckCursor.moveToFirst());
        TestUtils.validateRecord(insertCheckCursor, contentValues);

        testId = insertCheckCursor.getInt(insertCheckCursor.getColumnIndex(HotelsEntry._ID));

        Uri testUri = HotelsEntry.CONTENT_URI.buildUpon().appendPath(String.valueOf(testId)).build();

        retDelete = getContext().getContentResolver().delete(testUri, null, null);

        assertTrue(retDelete != 0);
        assertTrue(retDelete == 1);

        deleteCheckCursor = getContext().getContentResolver().query(HotelsEntry.CONTENT_URI, null, null, null, null);
        assertFalse("Items have not been deleted", deleteCheckCursor.moveToFirst());

        insertCheckCursor.close();
        deleteCheckCursor.close();
    }

    public void testRoomItemProviderDelete() {
        int testId;
        int retDelete;
        Cursor insertCheckCursor;
        Cursor deleteCheckCursor;
        ContentValues contentValues = TestUtils.getSingleRoomContentValues(getContext());


        getContext().getContentResolver().insert(RoomsEntry.CONTENT_URI, contentValues);
        insertCheckCursor = getContext().getContentResolver().query(RoomsEntry.CONTENT_URI, null, null, null, null);
        insertCheckCursor.moveToFirst();

        testId = insertCheckCursor.getInt(insertCheckCursor.getColumnIndex(RoomsEntry._ID));

        Uri testUri = RoomsEntry.CONTENT_URI.buildUpon().appendPath(String.valueOf(testId)).build();

        retDelete = getContext().getContentResolver().delete(testUri, null, null);

        assertTrue(retDelete != 0);
        assertTrue(retDelete == 1);

        deleteCheckCursor = getContext().getContentResolver().query(RoomsEntry.CONTENT_URI, null, null, null, null);
        assertFalse("Items have not been deleted", deleteCheckCursor.moveToFirst());

        insertCheckCursor.close();
        deleteCheckCursor.close();
    }

    public void testRoomListForHotelItemProviderDelete() {
        String testId;
        int retDelete;
        Cursor insertCheckCursor;
        Cursor deleteCheckCursor;
        ContentValues contentValues = TestUtils.getSingleRoomContentValues(getContext());


        getContext().getContentResolver().insert(RoomsEntry.CONTENT_URI, contentValues);
        getContext().getContentResolver().insert(RoomsEntry.CONTENT_URI, contentValues);
        getContext().getContentResolver().insert(RoomsEntry.CONTENT_URI, contentValues);


        insertCheckCursor = getContext().getContentResolver().query(RoomsEntry.CONTENT_URI, null, null, null, null);
        assertTrue(TAG + ", InsertTest: hotel cursor is empty", insertCheckCursor.moveToFirst());

        testId = contentValues.getAsString(RoomsEntry.HOTEL_ID);

        Uri testUri = RoomsEntry.CONTENT_URI.buildUpon().appendPath(DbContract.PATH_ROOMS_FOR_HOTEL_ITEM).appendPath(String.valueOf(testId)).build();
        //   Uri t = RoomsEntry.CONTENT_URI.buildUpon().

        retDelete = getContext().getContentResolver().delete(testUri, null, null);

        assertTrue(retDelete != 0);
        assertTrue(retDelete == 3);

        deleteCheckCursor = getContext().getContentResolver().query(RoomsEntry.CONTENT_URI, null, null, null, null);
        assertFalse("Items have not been deleted", deleteCheckCursor.moveToFirst());

        insertCheckCursor.close();
        deleteCheckCursor.close();
    }


}
