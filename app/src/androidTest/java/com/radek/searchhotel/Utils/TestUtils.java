package com.radek.searchhotel.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.test.AndroidTestCase;
import android.util.Log;

import com.radek.searchhotel.R;
import com.radek.searchhotel.model.DbContract.*;
import com.radek.searchhotel.model.DbHelper;

import java.io.ByteArrayOutputStream;
import java.util.Map;
import java.util.Set;


/**
 * Created by Radek on 2016-01-01.
 */
public class TestUtils extends AndroidTestCase {

    static int nextRating = 0;
    static int nextStar = -1;
    static int nextHotelId = 0;
    ContentValues valuesPackOne = new ContentValues();

    public static ContentValues[] getContentValues(int arrayLength) {
        ContentValues[] retContentValuesArray = new ContentValues[arrayLength];
        for (int i = 0; i < arrayLength; i++) {


            retContentValuesArray[i].put(HotelsEntry.TITLE_COLUMN, "Hotel: " + i);
            retContentValuesArray[i].put(HotelsEntry.COUNTRY_COLUMN, "Country: " + i);
            retContentValuesArray[i].put(HotelsEntry.CITY_COLUMN, "City: " + i);
            retContentValuesArray[i].put(HotelsEntry.STREET_COLUMN, "Street: " + i);
            retContentValuesArray[i].put(HotelsEntry.LATITUDE_COORD_COLUMN, (double) i + 100 / i); // example: 1.01 ; 2.02 ; 3.03
            retContentValuesArray[i].put(HotelsEntry.LONGITUDE_COORD_COLUMN, (double) i + 1000 / i); // example 1.001 ; 2.002 ; 3.003
            retContentValuesArray[i].put(HotelsEntry.RATING_COLUMN, (double) getNextRating() + 0.01); //
            retContentValuesArray[i].put(HotelsEntry.SHORT_DESCRIPTION_COLUMN, "Hotel: " + i);
            retContentValuesArray[i].put(HotelsEntry.STARS_COLUMN, TestUtils.getNextStar());
            retContentValuesArray[i].put(HotelsEntry.PICTURES_COLUMN, "Hotel: " + i);
            // MOD DATE

        }

        return retContentValuesArray;
    }

    public static ContentValues getSingleHotelContentValues(Context context) {
        ContentValues retContentValues = new ContentValues();

        final String TITLE_ENTRY = "Hotel title column entry";
        final String COUNTRY_ENTRY = "Hotel country column entry";
        final String CITY_ENTRY = "Hotel city column entry";
        final String STREET_ENTRY = "Hotel column street 53";
        final double LATITUDE_COORD_ENTRY = 111.11;
        final double LONGITUDE_COORD_ENTRY = -222.22;
        final double RATING_ENTRY = 3.33;
        final String DESCRIPTION_ENTRY = "Hotel description entry that is a little longer then others. " +
                "That is why we should write few sentences. That is what I am doing now";
        final int STARS_ENTRY = 4;
        final byte[] PICTURE_ENTRY = getTestImage(context);
        final String MODIFY_DATE = GeneralUtils.getModifyDate();

        retContentValues.put(HotelsEntry.TITLE_COLUMN, TITLE_ENTRY);
        retContentValues.put(HotelsEntry.COUNTRY_COLUMN, COUNTRY_ENTRY);
        retContentValues.put(HotelsEntry.CITY_COLUMN, CITY_ENTRY);
        retContentValues.put(HotelsEntry.STREET_COLUMN, STREET_ENTRY);
        retContentValues.put(HotelsEntry.LATITUDE_COORD_COLUMN, LATITUDE_COORD_ENTRY);
        retContentValues.put(HotelsEntry.LONGITUDE_COORD_COLUMN, LONGITUDE_COORD_ENTRY);
        retContentValues.put(HotelsEntry.RATING_COLUMN, RATING_ENTRY);
        retContentValues.put(HotelsEntry.SHORT_DESCRIPTION_COLUMN, DESCRIPTION_ENTRY);
        retContentValues.put(HotelsEntry.STARS_COLUMN, STARS_ENTRY);
        retContentValues.put(HotelsEntry.PICTURES_COLUMN, PICTURE_ENTRY);
        retContentValues.put(HotelsEntry.MODIFY_DATE, MODIFY_DATE);


        return retContentValues;
    }

    public static ContentValues getSingleRoomContentValues(Context context) {
        ContentValues retContentValues = new ContentValues();

        final int HOTEL_ID_ENTRY = 1;
        final String TITLE_ENTRY = "Room title column entry";
        final int CAPACITY_ENTRY = 2;
        final int PRICE_ENTRY = 300;
        final String DESCRIPTION_ENTRY = "Room description entry that is longer then title " +
                "It will probably say that this room is wonderful - the view definitely";
        final byte[] PICTURE_ENTRY = TestUtils.getTestImage(context);
        final String MODIFY_DATE = GeneralUtils.getModifyDate();

        retContentValues.put(RoomsEntry.HOTEL_ID, HOTEL_ID_ENTRY);
        retContentValues.put(RoomsEntry.TITLE_COLUMN, TITLE_ENTRY);
        retContentValues.put(RoomsEntry.CAPACITY_COLUMN, CAPACITY_ENTRY);
        retContentValues.put(RoomsEntry.PRICE_COLUMN, PRICE_ENTRY);
        retContentValues.put(RoomsEntry.DESCRIPTION_COLUMN, DESCRIPTION_ENTRY);
        retContentValues.put(RoomsEntry.PICTURES_COLUMN, PICTURE_ENTRY);
        retContentValues.put(HotelsEntry.MODIFY_DATE, MODIFY_DATE);

        return retContentValues;
    }


    public static void validateRecord(Cursor cursorToValidate, ContentValues contentValues) {

        Set<Map.Entry<String, Object>> testSet = contentValues.valueSet();

        for (Map.Entry<String, Object> entry : testSet) {
            String columnName = entry.getKey();
            int columnId = cursorToValidate.getColumnIndex(columnName);

            // how to validate blob?
            if (!entry.getKey().toString().equals(RoomsEntry.PICTURES_COLUMN)) {
                assertEquals(
                        "Value: " + entry.getValue().toString() + " is not equal to: " + cursorToValidate.getString(columnId),
                        entry.getValue().toString(),
                        cursorToValidate.getString(columnId));
            }
        }
    }


    public static int getNextHotelId() {
        return ++nextHotelId;
    }

    public static int getNextRating() {
        nextRating++;
        if (nextRating <= 5) {
            return nextRating;
        } else {
            nextRating = 0;
            return nextRating;
        }
    }

    public static int getNextStar() {
        nextStar++;
        if (nextStar <= 5) {
            return nextStar;
        } else {
            nextStar = -1;
            return nextStar;
        }
    }

    public static byte[] getTestImage(Context context) {
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.flower_test_image);
        if (bitmap == null) {
            Log.d("GET TEST IMAGE: ", "bitmap is null");
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);

        return stream.toByteArray();
    }

    public long insertSingleHotelRecord(Context testContext) {

        long retRowId;
        DbHelper dbHelper = new DbHelper(testContext);
        SQLiteDatabase testDatabase = dbHelper.getWritableDatabase();


        retRowId = testDatabase.insert(HotelsEntry.TABLE_NAME, null, valuesPackOne);


        return retRowId;
    }
}
