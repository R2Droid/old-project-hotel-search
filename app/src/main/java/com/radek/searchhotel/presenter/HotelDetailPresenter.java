package com.radek.searchhotel.presenter;

import android.database.Cursor;
import android.util.Log;

import com.radek.searchhotel.model.DbHelper;
import com.radek.searchhotel.model.ModelUtils;
import com.radek.searchhotel.view.HotelDetailActivity;

/**
 * Created by Radek on 2016-04-13.
 */
public class HotelDetailPresenter {
    private final String TAG = HotelDetailPresenter.class.getSimpleName();
    private ModelUtils mModelUtils;
    private HotelDetailActivity mView;
    private String currentHotelId;


    public HotelDetailPresenter(HotelDetailActivity context, String hotelId) {
        mModelUtils = new ModelUtils(context);
        this.currentHotelId = hotelId;

    }

    public void attachView(HotelDetailActivity view) {
        this.mView = view;
    }


    public Cursor setUpHotelDetail() {
        Cursor hotelDetailsCursor;

        hotelDetailsCursor = mModelUtils.getHotelDetailData(currentHotelId);
        Log.d(TAG, ": Setting up Hotel details");

        return hotelDetailsCursor;
    }

    public Cursor setUpRoomsDetails() {
        Log.d(TAG, ": Setting up Rooms details");

        return mModelUtils.getRoomListForHotelItem(currentHotelId);
    }


}
