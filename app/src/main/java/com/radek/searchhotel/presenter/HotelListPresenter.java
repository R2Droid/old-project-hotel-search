package com.radek.searchhotel.presenter;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.MergeCursor;
import android.util.ArrayMap;
import android.util.Log;
import android.widget.CursorAdapter;
import android.widget.Spinner;

import com.radek.searchhotel.view.CountryAdapter;
import com.radek.searchhotel.model.DbContract;
import com.radek.searchhotel.model.DbContract.*;
import com.radek.searchhotel.model.DbHelper;
import com.radek.searchhotel.view.HotelList;
import com.radek.searchhotel.model.ModelUtils;

import java.io.IOException;
import java.util.Map;

/**
 * Created by Radek on 2016-03-31.
 */
public class HotelListPresenter {
    private static final String TAG = HotelListPresenter.class.getSimpleName();
    private static final int FILTER_OPTIONS_COUNT = 11;

    private HotelList mView;
    private ModelUtils mModelUtils;
    private DbHelper dbHelper;
    private Context mContext;
    private CursorAdapter spinnerAdapter;

    public HotelListPresenter(HotelList context) {
        mModelUtils = new ModelUtils(context);
        mContext = context;
    }

    public void attachView(HotelList view) {

        this.mView = view;

    }

    public Cursor setUpHotelList() {
        dbHelper = new DbHelper(mContext);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }

        return mModelUtils.getHotelListData();
    }


    public void refreshHotelList(CursorAdapter hotelListAdapter, ArrayMap<String, String> filterOptions) {
        Cursor filteredData;
        StringBuilder selection = new StringBuilder();
        String[] selectionArgs = new String[filterOptions.size()];

        int i = 0;
        for (Map.Entry<String, String> v : filterOptions.entrySet()) {

            selection.append(v.getKey() + " = ? ");
            if (i < (filterOptions.size() - 1)) {
                selection.append(" AND ");
            }
            selectionArgs[i] = v.getValue();
            i++;
        }
        filteredData = mContext.getContentResolver().query(
                HotelsEntry.CONTENT_URI,
                null,
                selection.toString(),
                selectionArgs,
                null);


        hotelListAdapter.changeCursor(filteredData);
    }

    public void setUpCountrySpinner(Spinner countrySpinner) {


        Cursor countryCursor = mContext.getContentResolver().query(

                HotelsEntry.CONTENT_URI.buildUpon().appendPath(DbContract.PATH_COUNTRY_LIST).build(),
                null,
                null,
                null,
                null,
                null
        );

        String[] emptyRowColumn = new String[]{HotelsEntry._ID, HotelsEntry.COUNTRY_COLUMN};
        Object[] blankCountryValue = new Object[]{100, HotelsEntry.ALL_COUNTRIES_SPINNER_CHOICE};

        MatrixCursor emptyRow = new MatrixCursor(emptyRowColumn);
        emptyRow.addRow(blankCountryValue);

        MergeCursor mergeCursor = new MergeCursor(new Cursor[]{emptyRow, countryCursor});


        spinnerAdapter = new CountryAdapter(mContext, mergeCursor);
        countrySpinner.setAdapter(spinnerAdapter);

        // countryCursor.close();
    }

}
