package com.radek.searchhotel.model;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by Radek on 2015-12-18.
 */
public class DbContract {

    public static final String AUTHORITY = "com.radek.searchhotel.provider";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    public static final String PATH_HOTEL = "hotel";
    public static final String PATH_HOTEL_FILTER = "filter";
    public static final String PATH_COUNTRY_LIST = "country";

    public static final String PATH_ROOM = "room";
    public static final String PATH_ROOMS_FOR_HOTEL_ITEM = "for_hotel_item";

    public static String getIdFromUri(Uri uri) {

        return uri.getLastPathSegment();
    }

    public static abstract class HotelsEntry implements BaseColumns {

        public static final String ALL_COUNTRIES_SPINNER_CHOICE = "ALL COUNTRIES";

        // "content://com.radek.searchhotel.hotel"
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_HOTEL).build();

        public static final String CONTENT_DIR_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + AUTHORITY + "/" + PATH_HOTEL;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + AUTHORITY + "/" + PATH_ROOM;


        public static final String TABLE_NAME = "hotel";

        public static final String TITLE_COLUMN = "title";
        public static final String COUNTRY_COLUMN = "country";
        public static final String CITY_COLUMN = "city";
        public static final String STREET_COLUMN = "street";
        public static final String LATITUDE_COORD_COLUMN = "lat_coord";
        public static final String LONGITUDE_COORD_COLUMN = "lng_coord";
        public static final String RATING_COLUMN = "rating";
        public static final String SHORT_DESCRIPTION_COLUMN = "short_description";
        public static final String LONG_DESCRIPTION_COLUMN = "long_description";
        public static final String STARS_COLUMN = "stars";
        public static final String PICTURES_COLUMN = "pictures";
        public static final String FREE_WIFI_COLUMN = "free_wifi";
        public static final String FREE_PARKING_COLUMN = "free_parking";
        public static final String SWIMMING_POOL_COLUMN = "swimming_pool";
        public static final String SPORTS_FACILITY_COLUMN = "sports_facility";
        public static final String MODIFY_DATE = "modify_date";


        public static Uri buildHotelUri(long insertId) {
            return ContentUris.withAppendedId(CONTENT_URI, insertId);
        }

    }

    public static abstract class RoomsEntry implements BaseColumns {

        // "content://com.radek.searchhotel.room"
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_ROOM).build();


        public static final String CONTENT_DIR_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + AUTHORITY + "/" + PATH_ROOM;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + AUTHORITY + "/" + PATH_ROOM;


        public static final String TABLE_NAME = "room";

        public static final String HOTEL_ID = "hotel_id";
        public static final String TITLE_COLUMN = "title";
        public static final String CAPACITY_COLUMN = "capacity";
        public static final String PRICE_COLUMN = "price";
        public static final String DESCRIPTION_COLUMN = "description";
        public static final String PICTURES_COLUMN = "pictures";
        public static final String MODIFY_DATE = "modify_date";

        public static Uri buildRoomUri(long insertId) {
            return ContentUris.withAppendedId(CONTENT_URI, insertId);
        }


    }
}
