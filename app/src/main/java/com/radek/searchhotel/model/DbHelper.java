package com.radek.searchhotel.model;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.radek.searchhotel.model.DbContract.*;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Radek on 2015-12-18.
 */
public class DbHelper extends SQLiteOpenHelper {

    private static final String TAG = DbHelper.class.getSimpleName();
    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "hotel_search.db";
    private static String DATABASE_PATH = "";
    private Context mContext;


    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
        DATABASE_PATH = context.getDatabasePath(DATABASE_NAME).getPath();
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {


    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        db = SQLiteDatabase.openDatabase(DATABASE_PATH, null, SQLiteDatabase.OPEN_READWRITE);
    }

    private boolean isAlreadyExisting() {
        SQLiteDatabase tempDatabase = null;

        try {
            String databasePath = DATABASE_PATH;
            tempDatabase = SQLiteDatabase.openDatabase(databasePath, null, SQLiteDatabase.OPEN_READWRITE);
        } catch (SQLException e) {
            Log.e(TAG + ": DB does not exist", e.getMessage());
        }

        if (tempDatabase != null) {
            tempDatabase.close();
        }

        return tempDatabase != null;
    }


    private void copyDatabase() throws IOException {
        InputStream inputStream = mContext.getAssets().open(DATABASE_NAME);

        OutputStream outputStream = new FileOutputStream(DATABASE_PATH);

        byte[] buffer = new byte[1024];
        int length;

        while ((length = inputStream.read(buffer)) > 0) {
            outputStream.write(buffer, 0, length);
        }

        outputStream.flush();
        outputStream.close();
        inputStream.close();


    }

    public void createDataBase() throws IOException {
        boolean dbExist = isAlreadyExisting();

        if (dbExist) {

        } else {
            this.getReadableDatabase();
            try {
                copyDatabase();
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }


}
