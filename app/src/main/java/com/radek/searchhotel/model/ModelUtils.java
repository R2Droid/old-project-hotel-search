package com.radek.searchhotel.model;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.radek.searchhotel.model.DbContract.*;

/**
 * Created by Radek on 2016-04-04.
 */
public class ModelUtils {
    private final String TAG = ModelUtils.class.getSimpleName();
    private Context mContext;


    public ModelUtils(Context context) {
        mContext = context;
    }

    // GET HOTELS DATA
    public Cursor getHotelListData() {
        Cursor hotelListDataCursor;

        hotelListDataCursor = mContext.getContentResolver().query(HotelsEntry.CONTENT_URI, null, null, null, null);

        return hotelListDataCursor;
    }

    // GET HOTEL DATA
    public Cursor getHotelDetailData(String hotelId) {
        Cursor hotelDetailsCursor;
        Uri hotelDetailUri = HotelsEntry.CONTENT_URI.buildUpon().appendPath(hotelId).build();

        hotelDetailsCursor = mContext.getContentResolver().query(hotelDetailUri, null, null, null, null);

        return hotelDetailsCursor;
    }

    //GET ROOMS DATA FOR SPECIFIC HOTEL ITEM
    public Cursor getRoomListForHotelItem(String hotelId) {
        Cursor roomListCursor;

        Uri roomByHotelId = RoomsEntry.CONTENT_URI.buildUpon().appendPath(DbContract.PATH_ROOMS_FOR_HOTEL_ITEM).appendPath(hotelId).build();
        Uri allRooms = RoomsEntry.CONTENT_URI;
        Log.d(TAG, "Getting Room List for hotel_id: " + hotelId + "\n with uri: " + String.valueOf(roomByHotelId));

        roomListCursor = mContext.getContentResolver().query(roomByHotelId, null, null, null, null);

        return roomListCursor;
    }


}
