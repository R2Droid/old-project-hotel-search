package com.radek.searchhotel.model;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.radek.searchhotel.view.HotelDetailActivity;
import com.radek.searchhotel.utils.GeneralUtils;

import java.io.InputStream;

/**
 * Created by Radek on 2015-12-21.
 */
public class HotelsProvider extends ContentProvider {

    static final int HOTEL_LIST = 100;
    static final int HOTEL_ITEM = 101;
    static final int HOTEL_FILTER = 102;
    static final int COUNTRY_LIST = 103;
    static final int ROOM_LIST = 300;
    static final int ROOM_ITEM = 301;
    static final int ROOM_LIST_FOR_HOTEL_ITEM = 302;
    private static final String HOTEL_ID_EQUALS = "hotel_id = ?";
    private static final String ID_EQUALS = "_id = ?";
    private static final String[] COUNTRY_COLUMN = new String[]{DbContract.HotelsEntry.COUNTRY_COLUMN, DbContract.HotelsEntry._ID};
    private static final String HOTEL_DEFAULT_SORT_ORDER = "rating DESC";
    private static final String COUTRY_DEAFULT_SORT_ORDER = "country DESC";
    private static final String ROOM_DEFAULT_SORT_ORDER = "capacity ASC";

    private static final UriMatcher sUriMatcher = buildUriMatcher();
    private DbHelper mDbHelper;

    static UriMatcher buildUriMatcher() {
        final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = DbContract.AUTHORITY;

        uriMatcher.addURI(authority, DbContract.PATH_HOTEL, HOTEL_LIST);
        uriMatcher.addURI(authority, DbContract.PATH_HOTEL + "/#", HOTEL_ITEM);
        uriMatcher.addURI(authority, DbContract.PATH_HOTEL + "/" + DbContract.PATH_HOTEL_FILTER, HOTEL_FILTER);
        uriMatcher.addURI(authority, DbContract.PATH_HOTEL + "/" + DbContract.PATH_COUNTRY_LIST, COUNTRY_LIST);

        uriMatcher.addURI(authority, DbContract.PATH_ROOM, ROOM_LIST);
        uriMatcher.addURI(authority, DbContract.PATH_ROOM + "/#", ROOM_ITEM);
        uriMatcher.addURI(authority, DbContract.PATH_ROOM + "/" + DbContract.PATH_ROOMS_FOR_HOTEL_ITEM + "/#", ROOM_LIST_FOR_HOTEL_ITEM);

        return uriMatcher;
    }

    public static void saveImageToHotelTable(InputStream inputStream, Context context) {
        ContentValues contentValues = new ContentValues();
        long rowId;

        SQLiteOpenHelper sqLiteOpenHelper = new DbHelper(context);
        SQLiteDatabase sqLiteDatabase = sqLiteOpenHelper.getWritableDatabase();

        byte[] bytes = GeneralUtils.streamToByteArray(inputStream);
        contentValues.put(DbContract.HotelsEntry.PICTURES_COLUMN, bytes);


        rowId = sqLiteDatabase.insert(DbContract.HotelsEntry.TABLE_NAME, null, contentValues);

        if (rowId == -1) {
            throw new SQLiteException();
        }

    }

    @Override
    public boolean onCreate() {
        mDbHelper = new DbHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor retCursor;

        switch (sUriMatcher.match(uri)) {
            // "hotel"
            case HOTEL_LIST: {
                retCursor = mDbHelper.getReadableDatabase().query(
                        DbContract.HotelsEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        HOTEL_DEFAULT_SORT_ORDER // "rating DSC"
                );
                break;
            }
            //"hotel/filter"
            case HOTEL_FILTER: {
                retCursor = getHotelWithFilter(projection);
                break;
            }

            // "hotel/#"
            case HOTEL_ITEM: {
                retCursor = getHotelById(uri);
                break;
            }

            //"country"
            case COUNTRY_LIST:
                retCursor = mDbHelper.getReadableDatabase().query(
                        true,
                        DbContract.HotelsEntry.TABLE_NAME,
                        COUNTRY_COLUMN,
                        selection,
                        selectionArgs,
                        COUNTRY_COLUMN[0],
                        null,
                        COUTRY_DEAFULT_SORT_ORDER,
                        null);
                break;

            //"room"
            case ROOM_LIST:
                retCursor = mDbHelper.getReadableDatabase().query(
                        DbContract.RoomsEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        null
                );
                break;

            //"room/for_hotel_item/#
            case ROOM_LIST_FOR_HOTEL_ITEM:
                retCursor = getRoomByHotelId(uri);
                break;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return retCursor;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        long insertId;
        Uri retUri;

        switch (sUriMatcher.match(uri)) {
            case HOTEL_LIST:
                insertId = mDbHelper.getWritableDatabase().insert(DbContract.HotelsEntry.TABLE_NAME, null, values);

                if (insertId != -1L) {
                    retUri = DbContract.HotelsEntry.buildHotelUri(insertId);
                } else {
                    throw new android.database.SQLException("Failed to insert row into: " + uri);
                }
                break;

            case ROOM_LIST:
                insertId = mDbHelper.getWritableDatabase().insert(DbContract.RoomsEntry.TABLE_NAME, null, values);

                if (insertId != -1L) {
                    retUri = DbContract.RoomsEntry.buildRoomUri(insertId);
                } else {
                    throw new android.database.SQLException("Failed to insert row into: " + uri);
                }
                break;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return retUri;
    }


    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int retDeleted;


        switch (sUriMatcher.match(uri)) {
            case HOTEL_ITEM:
                retDeleted = deleteHotelItem(uri);
                break;
            case ROOM_LIST:
                retDeleted = deleteAllRooms();
                break;
            case ROOM_LIST_FOR_HOTEL_ITEM:
                retDeleted = deleteRoomsByHotelId(uri);
                break;
            case ROOM_ITEM:
                retDeleted = deleteRoomItem(uri);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (retDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return retDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {

        switch (sUriMatcher.match(uri)) {
            case HOTEL_LIST:
            case HOTEL_FILTER:
            case COUNTRY_LIST:
                return DbContract.HotelsEntry.CONTENT_DIR_TYPE;
            case HOTEL_ITEM:
                return DbContract.HotelsEntry.CONTENT_ITEM_TYPE;
            case ROOM_LIST:
            case ROOM_LIST_FOR_HOTEL_ITEM:
                return DbContract.RoomsEntry.CONTENT_DIR_TYPE;
            case ROOM_ITEM:
                return DbContract.RoomsEntry.CONTENT_ITEM_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: + uri");
        }
    }


    private Cursor getHotelWithFilter(String[] projection) {
        // TODO: waiting for MVP restructure

//        return mDbHelper.getReadableDatabase().query(
//                DbContract.HotelsEntry.TABLE_NAME,
//                projection,
//                filterSelection,
//                filterSelectionArgs,
//                null,
//                filterHaving,
//                filterSortOrder
//        );
        return null;
    }

    private Cursor getHotelById(Uri uri) {
        String[] selectionArgs = new String[]{
                DbContract.getIdFromUri(uri)
        };

        return mDbHelper.getReadableDatabase().query(
                DbContract.HotelsEntry.TABLE_NAME,
                null,
                ID_EQUALS, // "_id = ?"
                selectionArgs,
                null,
                null,
                null,
                null
        );
    }

    private Cursor getRoomByHotelId(Uri uri) {
        String[] selectionArgs = new String[]{
                DbContract.getIdFromUri(uri)
        };

        return mDbHelper.getReadableDatabase().query(DbContract.RoomsEntry.TABLE_NAME,
                null,
                HOTEL_ID_EQUALS, //"hotel_id = ?"
                selectionArgs,
                null,
                null,
                ROOM_DEFAULT_SORT_ORDER,
                null
        );
    }


    private int deleteHotelItem(Uri uri) {
        String itemId = DbContract.getIdFromUri(uri);

        return mDbHelper.getWritableDatabase().delete(
                DbContract.HotelsEntry.TABLE_NAME,
                ID_EQUALS,
                new String[]{itemId});
    }

    private int deleteRoomItem(Uri uri) {
        String[] whereArgs = new String[]{
                DbContract.getIdFromUri(uri)
        };

        return mDbHelper.getWritableDatabase().delete(
                DbContract.RoomsEntry.TABLE_NAME,
                ID_EQUALS,
                whereArgs
        );
    }

    private int deleteAllRooms() {
        return mDbHelper.getWritableDatabase().delete(DbContract.RoomsEntry.TABLE_NAME, null, null);
    }

    private int deleteRoomsByHotelId(Uri uri) {
        String[] whereArgs = HotelDetailActivity.getCurrentHotelId();

        return mDbHelper.getWritableDatabase().delete(
                DbContract.RoomsEntry.TABLE_NAME,
                HOTEL_ID_EQUALS,
                whereArgs
        );
    }


}



