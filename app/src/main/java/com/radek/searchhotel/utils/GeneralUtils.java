package com.radek.searchhotel.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.provider.MediaStore;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Toast;

import com.radek.searchhotel.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created by Radek on 2016-01-01.
 */
public class GeneralUtils {

    public static int RESULT_GET_IMG = 1;

    public static byte[] streamToByteArray(InputStream inputStream) {

        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];

        int length = 0;
        try {
            while ((length = inputStream.read(buffer)) != -1) {
                byteBuffer.write(buffer, 0, length);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return byteBuffer.toByteArray();
    }

    public static void getImageFromGallery(Activity activity) {
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");

        activity.startActivityForResult(galleryIntent, RESULT_GET_IMG);
    }

    public static void placeImagePreview(InputStream inputStream, ImageView imageVIew) {
//TODO: downsample the picture and then put it in the SQLite database

        Bitmap previewBitmap = BitmapFactory.decodeStream(inputStream);
        imageVIew.setImageBitmap(previewBitmap);
    }

    public static String getModifyDate() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault());

        return simpleDateFormat.format(new Date());
    }

}
