package com.radek.searchhotel.view;

import android.app.Activity;

import com.radek.searchhotel.model.DbContract.*;

import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import com.radek.searchhotel.R;
import com.radek.searchhotel.presenter.HotelDetailPresenter;

/**
 * Created by Radek on 2015-12-06.
 */
public class HotelDetailActivity extends Activity {
    private static String hotelId = "1";
    private HotelDetailPresenter mPresenter;
    private RoomsListAdapter roomsListAdapter;

    public static String[] getCurrentHotelId() {
        String[] hotelIdArgsArray = new String[]{
                hotelId
        };

        return hotelIdArgsArray;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_detail);

        setUpActivity();


    }

    public void setUpActivity() {
        String hotelId = getIntent().getStringExtra(Intent.EXTRA_TEXT);
        mPresenter = new HotelDetailPresenter(this, hotelId);
        TextView title = (TextView) findViewById(R.id.textview_hotel_detail_title);
        TextView description = (TextView) findViewById(R.id.textview_hotel_detail_details);

        Cursor hotelItem = mPresenter.setUpHotelDetail();
        hotelItem.moveToFirst();

        title.setText(hotelItem.getString(hotelItem.getColumnIndex(HotelsEntry.TITLE_COLUMN)));
        // description.setText(hotelItem.getString((hotelItem.getColumnIndex(HotelsEntry.LONG_DESCRIPTION_COLUMN))));

        Cursor roomsListForHotelItem = mPresenter.setUpRoomsDetails();
        roomsListForHotelItem.moveToFirst();

        ListView roomListView = (ListView) findViewById(R.id.hotel_details_rooms_list);
        roomsListAdapter = new RoomsListAdapter(this, roomsListForHotelItem);

        Log.w("ROOMS CURSOR DUMP", DatabaseUtils.dumpCursorToString(roomsListForHotelItem));

        roomListView.setAdapter(roomsListAdapter);
    }

}