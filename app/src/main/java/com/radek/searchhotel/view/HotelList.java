package com.radek.searchhotel.view;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.ArrayMap;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.radek.searchhotel.R;
import com.radek.searchhotel.model.DbContract;
import com.radek.searchhotel.presenter.HotelListPresenter;

import java.util.Iterator;
import java.util.Map;


/**
 * Created by Radek on 2015-12-05.
 */
public class HotelList extends Activity {
    private final ListView.OnItemClickListener mOnHotelItemClickListener = new ListView.OnItemClickListener() {
        // Start HotelDetailActivity on item click
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            enterHotelDetailActivity(id);

        }
    };
    EditText hotelNameEditText;
    Spinner countrySpinner;
    CheckBox freeWifiBox;
    CheckBox freeParkingBox;
    CheckBox swimmingPoolBox;
    CheckBox sportsFacilitiesBox;
    HotelListAdapter hotelListAdapter;
    private HotelListPresenter mViewPresenter;
    private final Button.OnClickListener filterButtonOnClickListener = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            mViewPresenter.refreshHotelList(hotelListAdapter, getFilterOptions());
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_list);

        setUpActivity();
    }

    // starting HotelDetailsActivity Activity
    private void enterHotelDetailActivity(long rowId) {
        Intent intent = new Intent(HotelList.this, HotelDetailActivity.class);
        intent.putExtra(Intent.EXTRA_TEXT, String.valueOf(rowId));
        startActivity(intent);
    }

    private void setUpActivity() {
        mViewPresenter = new HotelListPresenter(this);
        mViewPresenter.attachView(this);

        Cursor hotelsCursor = mViewPresenter.setUpHotelList();
        hotelListAdapter = new HotelListAdapter(this, hotelsCursor);

        ListView listHotels = (ListView) findViewById(R.id.listview_hotels);
        listHotels.setOnItemClickListener(mOnHotelItemClickListener);
        listHotels.setAdapter(hotelListAdapter);

        Button filterButton = (Button) findViewById(R.id.filter_button);
        filterButton.setOnClickListener(filterButtonOnClickListener);

        bindFilterDrawerViews();
        mViewPresenter.setUpCountrySpinner(countrySpinner);


    }

    private void bindFilterDrawerViews() {

        countrySpinner = (Spinner) findViewById(R.id.country_spinner);
        freeWifiBox = (CheckBox) findViewById(R.id.free_wifi_box);
        freeParkingBox = (CheckBox) findViewById(R.id.free_parking_box);
        swimmingPoolBox = (CheckBox) findViewById(R.id.swimming_pool_box);
        sportsFacilitiesBox = (CheckBox) findViewById(R.id.sports_facilities_box);

    }

    private ArrayMap<String, String> getFilterOptions() {
        ArrayMap<String, String> filterOptions = new ArrayMap<>();

        TextView selectedCountry = (TextView) countrySpinner.getSelectedView();
        if (!selectedCountry.getText().toString().equals(DbContract.HotelsEntry.ALL_COUNTRIES_SPINNER_CHOICE)) {
            filterOptions.put(DbContract.HotelsEntry.COUNTRY_COLUMN, String.valueOf(selectedCountry.getText().toString()));
        }

        filterOptions.put(DbContract.HotelsEntry.FREE_WIFI_COLUMN, String.valueOf(freeWifiBox.isChecked() ? 1 : 0));
        filterOptions.put(DbContract.HotelsEntry.FREE_PARKING_COLUMN, String.valueOf(freeParkingBox.isChecked() ? 1 : 0));
        filterOptions.put(DbContract.HotelsEntry.SWIMMING_POOL_COLUMN, String.valueOf(swimmingPoolBox.isChecked() ? 1 : 0));
        filterOptions.put(DbContract.HotelsEntry.SPORTS_FACILITY_COLUMN, String.valueOf(sportsFacilitiesBox.isChecked() ? 1 : 0));


        for (Iterator<Map.Entry<String, String>> iterator =
             filterOptions.entrySet().iterator();
             iterator.hasNext(); ) {

            Map.Entry<String, String> entry = iterator.next();

            if (entry.getValue().equals("0")) {
                iterator.remove();
            }
        }
        return filterOptions;
    }

}

