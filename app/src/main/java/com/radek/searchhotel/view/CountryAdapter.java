package com.radek.searchhotel.view;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.radek.searchhotel.R;
import com.radek.searchhotel.model.DbContract;

/**
 * Created by Radek on 2016-07-28.
 */
public class CountryAdapter extends CursorAdapter {

    public CountryAdapter(Context context, Cursor c) {
        super(context, c, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.item_country_spinner, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView textView = (TextView) view.findViewById(R.id.country_spinner_item);

        textView.setText(cursor.getString(cursor.getColumnIndex(DbContract.HotelsEntry.COUNTRY_COLUMN)));
    }
}
