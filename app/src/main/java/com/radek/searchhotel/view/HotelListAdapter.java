package com.radek.searchhotel.view;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.radek.searchhotel.R;
import com.radek.searchhotel.model.DbContract;


public class HotelListAdapter extends CursorAdapter {

    public HotelListAdapter(Context context, Cursor c) {
        super(context, c, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.item_hotel_listview, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView textView = (TextView) view.findViewById(R.id.listview_item_main_short_desc);

        String title = cursor.getString(cursor.getColumnIndex(DbContract.HotelsEntry.TITLE_COLUMN));
        String shortDescription = cursor.getString(cursor.getColumnIndex(DbContract.HotelsEntry.SHORT_DESCRIPTION_COLUMN));
        String country = cursor.getString(cursor.getColumnIndex(DbContract.HotelsEntry.COUNTRY_COLUMN));
        String city = cursor.getString(cursor.getColumnIndex(DbContract.HotelsEntry.CITY_COLUMN));
        String rating = cursor.getString(cursor.getColumnIndex(DbContract.HotelsEntry.RATING_COLUMN));

        String listItemText = title + " (" + rating + "/10)" + "\n\n" + country + ",\n" + city + "\n\n" + shortDescription;
        textView.setText(listItemText);
    }
}
