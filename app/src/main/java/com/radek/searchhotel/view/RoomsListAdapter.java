package com.radek.searchhotel.view;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;

import com.radek.searchhotel.model.DbContract.*;

import android.widget.TextView;

import com.radek.searchhotel.R;

/**
 * Created by Radek on 2016-05-03.
 */
public class RoomsListAdapter extends CursorAdapter {

    public RoomsListAdapter(Context context, Cursor c) {
        super(context, c, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.item_room_listview, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView roomTitle = (TextView) view.findViewById(R.id.room_title);
        TextView roomDescription = (TextView) view.findViewById(R.id.room_desc);

        roomTitle.setText(cursor.getString(cursor.getColumnIndex(RoomsEntry.TITLE_COLUMN)));

        StringBuilder roomDetails = new StringBuilder();

        roomDetails.append(cursor.getString(cursor.getColumnIndex(RoomsEntry.DESCRIPTION_COLUMN)) + "\n");
        roomDetails.append("Price: " + cursor.getString(cursor.getColumnIndex(RoomsEntry.PRICE_COLUMN)) + "\n");
        roomDetails.append("Capacity: " + cursor.getString(cursor.getColumnIndex(RoomsEntry.CAPACITY_COLUMN)) + "\n");

        roomDescription.setText(roomDetails.toString());
    }
}
